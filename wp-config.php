<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //



/** Database Charset to use in creating database tables. */




 /**
  * Production
  */
// if($_SERVER['SERVER_NAME'] == "buildwithbankers.com" || $_SERVER['SERVER_NAME'] == "www.buildwithbankers.com") {
// 	define( 'DB_NAME', 'bwb_live' );

// 	/** MySQL database username */
// 	define( 'DB_USER', 'corpmktadmin@corpmarketing-prod-mysql.mysql.database.azure.com' );

// 	/** MySQL database password */
// 	define( 'DB_PASSWORD', 'Blueclouds@727' );

// 	/** MySQL hostname */
// 	define( 'DB_HOST', 'corpmarketing-prod-mysql.mysql.database.azure.com' );
    
	define('WP_CONTENT_DIR', __DIR__ . '/wp-content');
// } 
 /**
  * Staging
  */
if($_SERVER['SERVER_NAME'] == "pardot-file-manager.azurewebsites.net") {
	define('FORCE_SSL_ADMIN', true);
	$_SERVER['HTTPS']='on';
	define('WP_CONTENT_URL', 'https://' . $_SERVER['SERVER_NAME'] . '/wp-content');
	define('WP_SITEURL', 'https://' . $_SERVER['SERVER_NAME'] . '/');
	define('WP_HOME', 'https://' . $_SERVER['SERVER_NAME']);
	/** The name of the database for WordPress */
	define( 'DB_NAME', 'pardot_file_manager' );

	/** MySQL database username */
	define( 'DB_USER', 'corpmktadmin@corpmarketing-prod-mysql.mysql.database.azure.com' );

	/** MySQL database password */
	define( 'DB_PASSWORD', 'Blueclouds@727' );

	/** MySQL hostname */
	define( 'DB_HOST', 'corpmarketing-prod-mysql.mysql.database.azure.com' );
    
}
 /**
  * Local
  */
elseif($_SERVER['SERVER_NAME'] == 'pardot-file-manager.test') {
	// ** MySQL settings - You can get this info from your web host ** //
	/** The name of the database for WordPress */
	define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/wp-content');
	define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/');
	define('WP_HOME', 'http://' . $_SERVER['SERVER_NAME']);


	define( 'DB_NAME', 'pardot_file_manager' );

	/** MySQL database username */
	define( 'DB_USER', 'root' );

	/** MySQL database password */
	define( 'DB_PASSWORD', 'Bankers@1' );


	/** MySQL hostname */
	define( 'DB_HOST', '127.0.0.1' );
}
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('FS_METHOD','direct');

define( 'WP_DEFAULT_THEME', 'bankers' );
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'put your unique phrase here' );
define( 'SECURE_AUTH_KEY',  'put your unique phrase here' );
define( 'LOGGED_IN_KEY',    'put your unique phrase here' );
define( 'NONCE_KEY',        'put your unique phrase here' );
define( 'AUTH_SALT',        'put your unique phrase here' );
define( 'SECURE_AUTH_SALT', 'put your unique phrase here' );
define( 'LOGGED_IN_SALT',   'put your unique phrase here' );
define( 'NONCE_SALT',       'put your unique phrase here' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
