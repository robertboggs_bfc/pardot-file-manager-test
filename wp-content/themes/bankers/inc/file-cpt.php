<?php 
add_action( 'init', 'files_cpt' );
/**
 * Register a File post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function files_cpt() {
	$labels = array(
		'name'               => _x( 'Files', 'post type general name' ),
		'singular_name'      => _x( 'File', 'post type singular name' ),
		'menu_name'          => _x( 'Files', 'admin menu' ),
		'name_admin_bar'     => _x( 'File', 'add new on admin bar' ),
		'add_new'            => _x( 'Add New', 'File' ),
		'add_new_item'       => __( 'Add New File' ),
		'new_item'           => __( 'New File' ),
		'edit_item'          => __( 'Edit File' ),
		'view_item'          => __( 'View File' ),
		'all_items'          => __( 'All Files' ),
		'search_items'       => __( 'Search Files' ),
		'parent_item_colon'  => __( 'Parent Files:' ),
		'not_found'          => __( 'No Files found.' ),
		'not_found_in_trash' => __( 'No Files found in Trash.' )
	);

	$args = array(
		'labels'             => $labels,
		'description'        => __( 'Description.' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
        'show_in_menu'       => true,
        'show_in_rest'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'File' ),
		'capability_type'    => 'post',
		'has_archive'        => 'files',
		'taxonomies'				 => array('category', 'post_tag'),
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title' )
	);

    register_post_type( 'file', $args );
    
    	// Add new taxonomy, NOT hierarchical (like tags)
	$cat_labels = array(
		'name'                       => _x( 'Categories', 'taxonomy general name', 'textdomain' ),
		'singular_name'              => _x( 'Category', 'taxonomy singular name', 'textdomain' ),
		'search_items'               => __( 'Search Categories', 'textdomain' ),
		'popular_items'              => __( 'Popular Categories', 'textdomain' ),
		'all_items'                  => __( 'All Categories', 'textdomain' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Category', 'textdomain' ),
		'update_item'                => __( 'Update Category', 'textdomain' ),
		'add_new_item'               => __( 'Add New Category', 'textdomain' ),
		'new_item_name'              => __( 'New Writer Name', 'textdomain' ),
		'separate_items_with_commas' => __( 'Separate categories with commas', 'textdomain' ),
		'add_or_remove_items'        => __( 'Add or remove categories', 'textdomain' ),
		'choose_from_most_used'      => __( 'Choose from the most used categories', 'textdomain' ),
		'not_found'                  => __( 'No categoriess found.', 'textdomain' ),
		'menu_name'                  => __( 'Categories', 'textdomain' ),
	);

	$cat_args = array(
		'hierarchical'          => true,
		'labels'                => $cat_labels,
		'show_ui'               => true,
        'show_admin_column'     => true,
        'show_in_rest'          => true,
        'rest_base'             => 'category',
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'category' ),
	);

	// register_taxonomy( 'file_category', 'file', $cat_args );

  $camp_labels = array(
		'name'                       => _x( 'Categories', 'taxonomy general name', 'textdomain' ),
		'singular_name'              => _x( 'Category', 'taxonomy singular name', 'textdomain' ),
		'search_items'               => __( 'Search Categories', 'textdomain' ),
		'popular_items'              => __( 'Popular Categories', 'textdomain' ),
		'all_items'                  => __( 'All Categories', 'textdomain' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Category', 'textdomain' ),
		'update_item'                => __( 'Update Category', 'textdomain' ),
		'add_new_item'               => __( 'Add New Category', 'textdomain' ),
		'new_item_name'              => __( 'New Writer Name', 'textdomain' ),
		'separate_items_with_commas' => __( 'Separate categories with commas', 'textdomain' ),
		'add_or_remove_items'        => __( 'Add or remove categories', 'textdomain' ),
		'choose_from_most_used'      => __( 'Choose from the most used categories', 'textdomain' ),
		'not_found'                  => __( 'No categoriess found.', 'textdomain' ),
		'menu_name'                  => __( 'Categories', 'textdomain' ),
	);

	$camp_args = array(
		'hierarchical'          => false,
		'labels'                => $camp_labels,
		'show_ui'               => true,
        'show_admin_column'     => true,
        'show_in_rest'          => true,
        'rest_base'             => 'category',
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'category' ),
	);

	// register_taxonomy( 'file_category', 'files', $camp_args );
}


// add_action( 'acf/save_post', 'update_pardot', 20, 1 );
// add_filter('acf/pre_save_post' , 'update_pardot', 10, 1 );
 
function update_pardot( $post_id ) {	
	if(get_post_type($post_id) === 'file') {
		$image = get_field('image', $post_id);
		$pardot_id = get_field('pardot_id', $post_id);
		$image_file_str = file_get_contents($image['url']);
		
		if ( $pardot_id ) {
			$file_data = update_pardot_file();
		} else {
			$file_data = create_pardot_file($image, $image_file_str);

			$file_data = json_decode($file_data, true);

			$pardot_id_obj = get_field_object('pardot_id', $post_id);
			$pardot_url_obj = get_field_object('pardot_url', $post_id);
			$pardot_name_obj = get_field_object('pardot_name', $post_id);

			update_field($pardot_id_obj['key'], $file_data['id'], $post_id);
			update_field($pardot_url_obj['key'], $file_data['url'], $post_id);
			update_field($pardot_name_obj['key'], $file_data['name'], $post_id);
		}
	}
}
