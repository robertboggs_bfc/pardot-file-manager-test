<?php
function build_content($content) {
    ob_start(); ?>
    <div class="wysiwyg-content">
        <?php echo $content;?>
    </div>
    <?php 
    echo ob_get_clean();
}