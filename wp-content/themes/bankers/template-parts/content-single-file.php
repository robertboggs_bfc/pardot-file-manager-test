<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Bankers
 */
$img = get_field('image');
$pardot_url = get_field('pardot_url');
$pardot_name = get_field('pardot_name');
$file_categories = wp_get_post_categories(get_the_ID(), array('fields'=>'all'));
$file_tags = wp_get_post_tags(get_the_ID(), array('fields'=>'all'));
?>	
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <h2><?php echo $pardot_name;?></h2>
  <?php if($file_categories):?>
    <div class="single-file--categories">
      Categories:
      <ul>
        <?php foreach($file_categories as $cat):?>
          <li>
            <a href="<?php echo get_category_link($cat);?>">
              <?php echo $cat->name;?>
            </a>
          </li>
        <?php endforeach;?>
      </ul>
    </div>
  <?php endif;?> 
  <?php if($file_tags):?>
    <div class="single-file--tags">
      Tags:
      <?php foreach($file_tags as $tag):?>
        <a href="<?php echo get_tag_link($tag);?>"><?php echo $tag->name;?></a>
      <?php endforeach;?>
    </div>
  <?php endif;?> 
  <hr>
  <div class="single-file--image-container">
    <img class="single-file--image" src="<?php echo $img['url'];?>" alt="<?php echo $img['alt'];?>" />
  </div>
	<div class="single-file--quick-link"><input type="text" disabled value="<?php echo $pardot_url;?>" title="<?php echo $pardot_url;?>"><button class="copyLink fa fa-copy" title="Copy File URL"></button></div>
</article><!-- #post-<?php the_ID(); ?> -->
