<aside id="sidebar">
    <div class="sidebar--inner">
        <?php if(is_single()):?>
            <?php if(get_field('sidebar_type', 'options') == 'form'):?>
                <?php echo do_shortcode(get_field('sidebar_form', 'options'));?>
            <?php endif;?>
        <?php else:?>
            <?php if(get_field('sidebar_type') == 'form'):?>
                <?php echo do_shortcode(get_field('sidebar_form'));?>
            <?php endif;?>
        <?php endif;?>
    </div>
</aside>