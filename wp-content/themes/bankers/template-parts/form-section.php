<?php
    $headline = get_sub_field('headline');
    $content = get_sub_field('content');
    $section_classes = 'form-section';
    $form = get_sub_field('form_shortcode');

    ob_start();
?>
    <div class="container">
        <?php if($headline['show_headline'] && $headline['copy']):?>
            <h2 class="<?php echo $headline['alignment'];?>"><?php echo $headline['copy'];?></h2>
        <?php endif;?>
        <?php build_content($content);?>
        <?php echo do_shortcode($form);?>
    </div>
<?php 
    build_section(ob_get_clean(), $section_classes);