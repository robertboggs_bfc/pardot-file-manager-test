<?php
    $headline = get_sub_field('headline');
    $section_classes = 'hero-simple-section';

    $background = get_sub_field('background');

    ob_start();
?>
<div class="bg-image" style="background-image:url(<?php echo $background['url'];?>);"></div>
    <div class="container">
        <?php if($headline['show_headline'] && $headline['copy']):?>
            <h1 class="<?php echo $headline['alignment'];?>"><?php echo $headline['copy'];?></h1>
        <?php endif;?>
    </div>
<?php 
    build_section(ob_get_clean(), $section_classes);