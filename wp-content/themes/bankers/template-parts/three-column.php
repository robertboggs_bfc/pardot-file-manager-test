<?php
    $headline = get_sub_field('headline');
    $section_classes = 'two-column-section';
    $left_column = get_sub_field('left_column');
    $middle_column = get_sub_field('middle_column');
    $right_column = get_sub_field('right_column');

    ob_start();
?>
    <div class="container">
        <?php if($headline['show_headline'] && $headline['copy']):?>
            <h2 class="<?php echo $headline['alignment'];?>"><?php echo $headline['copy'];?></h2>
        <?php endif;?>
        <div class="row">
            <div class="left-column col-sm-4">
                <?php build_content($left_column['content']);?>
            </div>
            <div class="middle-column col-sm-4">
                <?php build_content($middle_column['content']);?>
            </div>
            <div class="left-column col-sm-4">
                <?php build_content($right_column['content']);?>
            </div>
        </div>
    </div>
<?php 
    build_section(ob_get_clean(), $section_classes);