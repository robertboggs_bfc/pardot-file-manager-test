<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Bankers
 */
$img = get_field('image');
$pardot_url = get_field('pardot_url');
$pardot_name = get_field('pardot_name');
?>	
<article id="post-<?php the_ID(); ?>" <?php post_class('file-tile'); ?>>
	<a class="file-tile--click" href="<?php the_permalink();?>">
		<h2><?php echo $pardot_name;?></h2>
		<img class="file-tile--image" src="<?php echo $img['url'];?>" alt="<?php echo $img['alt'];?>" />	
	</a>
	<div class="file-tile--quick-link"><input type="text" disabled value="<?php echo $pardot_url;?>" title="<?php echo $pardot_url;?>"><button class="copyLink fa fa-copy" title="Copy File URL"></button></div>
</article><!-- #post-<?php the_ID(); ?> -->
