(function($) {
  $('.copyLink').on('click', function(event) {
    var tar_parent = $(event.target).parent(),
        link = tar_parent.find('input').val();
    navigator.clipboard.writeText(link).then(function() {
        confirm_link_copied(tar_parent);
        console.log('copied');
      },
      function() {
        console.log('write failed');
      });
  });
  function confirm_link_copied(tar) {
    $(tar).addClass('copy-confirmed');
    console.log('notified');
    setTimeout(function() {
      $(tar).removeClass('copy-confirmed');
    }, 2000);
  }
})(jQuery);