<?php
/**
 * Bankers functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Bankers
 */

if ( ! function_exists( 'bankers_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function bankers_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Bankers, use a find and replace
		 * to change 'bankers' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'bankers', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );


		register_nav_menus( array(
			'mainNav' => __( 'Main Nav', 'bankers' ),
			'HomeownersSubNav' => __( 'Homeowners Sub Nav', 'bankers' ),
			'BusinessSubNav' => __( 'Business Sub Nav', 'bankers' ),
			'BottomNav' => __( 'BottomNav', 'bankers' ),
			'FAQNav' => __( 'FAQ Nav', 'bankers' ),
		) );
		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'bankers_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'bankers_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function bankers_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'bankers_content_width', 640 );
}
add_action( 'after_setup_theme', 'bankers_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function bankers_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'bankers' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'bankers' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'bankers_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function bankers_scripts() {
	wp_enqueue_style('bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');

	wp_enqueue_style('font-awesome', '//use.fontawesome.com/releases/v5.6.3/css/all.css');

	// wp_enqueue_style( 'bankers-style', get_stylesheet_uri() );

	wp_enqueue_style( 'main-stylesheet', get_template_directory_uri() . '/css/style.css');

	wp_enqueue_script( 'bankers-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'bankers-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'main-scripts', get_template_directory_uri().'/js/scripts.js', array('jquery'), '', true);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'bankers_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

add_action('acf/init', 'my_acf_init');
function my_acf_init() {
	
	// check function exists
	if( function_exists('acf_register_block') ) {
		
		// register a testimonial block
		acf_register_block(array(
			'name'				=> 'test-block',
			'title'				=> __('Test Block'),
			'description'		=> __('A custom testing block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'formatting',
			'icon'				=> 'admin-comments',
			'keywords'			=> array( 'test', 'quote' ),
		));
	}
}
function my_acf_block_render_callback( $block ) {
	
	// convert name ("acf/testimonial") into path friendly slug ("testimonial")
	$slug = str_replace('acf/', '', $block['name']);
	
	// include a template part from within the "template-parts/block" folder
	if( file_exists( get_theme_file_path("/template-parts/block/{$slug}.php") ) ) {
		include( get_theme_file_path("/template-parts/block/{$slug}.php") );
	}
}
/**
 * Add In ACF Options Pages
 */
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}
/**
 * Slugify func
 */
function slugify($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}
function my_custom_mime_types( $mimes ) {
	// New allowed mime types.
	$mimes['svg'] = 'image/svg+xml';
	$mimes['svgz'] = 'image/svg+xml';
	$mimes['doc'] = 'application/msword';
return $mimes;
}
add_filter( 'upload_mimes', 'my_custom_mime_types' );

// Response Mail Gforms
function form_response($entry, $form) {
	$opts = array(
		'http'=>array(
			'method'=>"GET",
			'header'=>"Accept-language: en\r\n"
		)
	);
	
	$context = stream_context_create($opts);
	
	// Open the file using the HTTP headers set above
	$file = file_get_contents(get_template_directory_uri().'/templates/mail/test1.php', false, $context);
	
	$to = rgar($entry, '2');
	$subject = 'The subject';
	$body = $file;
	$headers = array('Content-Type: text/html; charset=UTF-8');
	$attachments = array(WP_CONTENT_DIR . '/uploads/2019/03/Builders-Risk-Info.pdf');
	
	wp_mail( $to, $subject, $body, $headers, $attachments );
}
// add_action('gform_after_submission', 'form_response', 10, 2);

//ajax email response
function ajax_form_response() {
	$opts = array(
		'http'=>array(
			'method'=>"GET",
			'header'=>"Accept-language: en\r\n"
		)
	);
	
	$context = stream_context_create($opts);
	
	// Open the file using the HTTP headers set above
	$file = file_get_contents(get_template_directory_uri().'/templates/mail/test1.php', false, $context);
	
	$to = $_POST['email'];
	$subject = 'The subject';
	$body = $file;
	$headers = array('Content-Type: text/html; charset=UTF-8');
	$attachments = array(WP_CONTENT_DIR . '/uploads/2019/03/Builders-Risk-Info.pdf');
	
	wp_mail( $to, $subject, $body, $headers, $attachments );
}
add_action('wp_ajax_nopriv_form_response', 'ajax_form_response'); 
add_action('wp_ajax_form_response', 'ajax_form_response'); 

//post excerpt filter
function custom_excerpt_more($more) {
	global $post;
	$more_text = '<a href="'. get_permalink($post->ID) . '" class="button readmore">' . 'Read More' . '</a>';
 }
 add_filter('excerpt_more', 'custom_excerpt_more');
function replace_excerpt($content) {
	return str_replace('&hellip;',
			'',
			$content
	);
}
add_filter('the_excerpt', 'replace_excerpt');
//Big Sea Related posts Func

function bsd_get_related_posts($excludeArr, $limit = 3)
{

    $args = array(
		'post_type' => 'post',
        'posts_per_page' => $limit,
        'post__not_in' => $excludeArr
    );

    $postTags = wp_get_post_tags($post->ID);
    if ($postTags) {
        foreach ($postTags as $tag) {
            $tagArrayStringified .= $tag->slug . ',';
        }

        $args['tag'] = $tagArrayStringified;
    }

    return get_posts($args);
}

if ( function_exists( 'add_image_size' ) ) { 
    add_image_size('blog-medium', 720, 432, true);
}

add_filter( 'gform_form_tag', 'form_tag', 10, 2 );
function form_tag( $form_tag, $form ) {
	$form_title = $form['title'];
    $form_tag = preg_replace( "/\<form{1}/", "<form aria-label='".$form_title."'", $form_tag );
    return $form_tag;
}

add_filter( 'gform_field_content', 'field_tag', 10, 2 );
function field_tag( $field_content, $field ) {
	$field_label = $field['label'];
	$field_content = str_replace( '<input', "<input aria-label='".$field_label."' ", $field_content );
	$field_content = str_replace( '<select', "<select aria-label='".$field_label."' ", $field_content );
	$field_content = str_replace( '<textarea', "<textarea aria-label='".$field_label."' ", $field_content );
	return $field_content;
}

function my_modify_main_query( $query ) {
	if ( $query->is_home() && $query->is_main_query() || $query->is_archive() && $query->is_main_query()) { // Run only on the homepage
		$query->query_vars['post_type'] = 'file'; // Exclude my featured category because I display that elsewhere
		$query->query_vars['posts_per_page'] = 100; // Show only 5 posts on the homepage only
	}
}
	// Hook my above function to the pre_get_posts action
add_action( 'pre_get_posts', 'my_modify_main_query' );

function get_token() {
	$data = array(
		"grant_type" => "password",
		"client_id" => "3MVG9y6x0357HleeuYnP8xYm1FewYzgYdWsxQoGD17rG.ijYrRdsHMVbxEJ4xGH4HW8f..69sztAFaWIT4LCe",
		"client_secret" => "62EE98F7FDE3B0B38BCF5A32860587543B9E4714B716107F25D4C14436F0DFBE",
		"username" => "findagent@bankersfinancialcorp.com",
		"password" => "Bankers@1USvvCYIMMGoPTARm3EiLbAnH"
	);

	$url = 'https://login.salesforce.com/services/oauth2/token';
	$fields_string = "";

	//url-ify the data for the POST
	foreach($data as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
	$fields_string = rtrim($fields_string,'&');
	$headers = array(
        'Content-type' => 'application/x-www-form-urlencoded;charset=UTF-8'
    );
	//open connection
	$ch = curl_init();

	//set the url, number of POST vars, POST data
    
	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch,CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch,CURLOPT_POST,count($data));
	curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);

	//execute post
	$result = curl_exec($ch);

	//close connection
	curl_close($ch);
	return json_decode($result, true);
}



//// Create new Pardot File

	function create_pardot_file($image_fields, $file) {
	$token_data = get_token();
	$pardot_business_unit_id = '0Uv440000004C9cCAE';
	// data fields for POST request
	$fields = '{"name": "'.$image_fields['filename'].'", "campaignId": 22637}';

	// URL to upload to
	$url = 'https://pi.pardot.com/api/v5/objects/files?fields=id,name,url,createdBy.id,createdBy.username';


	// curl

	$curl = curl_init();

	// $url_data = http_build_query($data);

	$boundary = uniqid();
	$delimiter = '-------------' . $boundary;

	$post_data = build_data_files($boundary, $fields, $file, $image_fields['filename']);

	
	curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		//CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POST => 1,
		CURLOPT_POSTFIELDS => $post_data,
		CURLOPT_HTTPHEADER => array(
			"Authorization: Bearer ".$token_data['access_token'],
			"Content-Type: multipart/form-data; boundary=" . $delimiter,
			"Content-Length: " . strlen($post_data),
			"Pardot-Business-Unit-Id: ".$pardot_business_unit_id

		),

		
	));


	//
	$response = curl_exec($curl);

	$info = curl_getinfo($curl);
	//echo "code: ${info['http_code']}";

	//print_r($info['request_header']);

	// var_dump($response);
	// $err = curl_error($curl);

	// echo "error";
	// var_dump($err);
	curl_close($curl);
	return $response;
}


function build_data_files($boundary, $fields, $file, $filename){
    $data = '';
    $eol = "\r\n";

		//Open
    $delimiter = '-------------' . $boundary;

		//Add Input fields
		$data .= "--" . $delimiter . $eol
				. 'Content-Disposition: form-data; name="input"'.$eol.$eol
				. $fields . $eol;


		//Add File 
		$data .= "--" . $delimiter . $eol
				. 'Content-Disposition: form-data; name="file"; filename="' . $filename . '"' . $eol
				//. 'Content-Type: image/png'.$eol
				. 'Content-Transfer-Encoding: binary'.$eol
				;

		$data .= $eol;
		$data .= $file . $eol;
    
		//Close
    $data .= "--" . $delimiter . "--".$eol;


    return $data;
}

//// Update Pardot File
function update_pardot_file() {
	return 'Not Implemented yet!';
}

//// Delete Pardot File
function delete_pardot_file() {
	return 'Not Implemented Yet!';
}

//// Get Single Pardot File
function get_pardot_file() {
	return 'Not Implementeed Yet!';
}